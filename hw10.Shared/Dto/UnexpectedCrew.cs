using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace hw10.Dto
{
    [DataContract]
    public partial class UnexpectedCrewDto
    {
        /// <summary>
        /// Gets or Sets Id
        /// </summary>
        [DataMember(Name="id")]
        public long? Id { get; set; }

        /// <summary>
        /// Gets or Sets Pilot
        /// </summary>
        [DataMember(Name="pilot")]
        public IEnumerable<PilotDto> Pilots { get; set; }

        /// <summary>
        /// Gets or Sets Stewardesses
        /// </summary>
        [DataMember(Name="stewardess")]
        public IEnumerable<StewardessDto> Stewardesses { get; set; }
    }
}
