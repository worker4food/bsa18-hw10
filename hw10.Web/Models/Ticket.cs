using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace hw10.Models
{
    /// <summary>
    /// Ticket
    /// </summary>
    public partial class Ticket : Entity<long>
    {
        /// <summary>
        /// Gets or Sets Price
        /// </summary>
        public decimal Price { get; set; }

        public string FlightId { get; set; }
        /// <summary>
        /// Gets or Sets FlightId
        /// </summary>
        public Flight Flight { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Ticket {\n");
            sb.Append("  Id: ").Append(Id).Append("\n");
            sb.Append("  Price: ").Append(Price).Append("\n");
            sb.Append("  Flight: ").Append(Flight).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
    }
}
