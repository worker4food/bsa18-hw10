
namespace hw10.Models
{
    public class Error
    {
        public string Type { get; set; }
        public string Message { get; set; }
    }
}
