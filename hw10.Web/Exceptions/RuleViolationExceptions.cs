using System;

namespace hw10.Exceptions
{
    public class RuleViolationException : ApplicationException
    {
        public RuleViolationException(string message) : base(message)
        { }

        public RuleViolationException(string message, Exception innerException) : base(message, innerException)
        { }
    }
}
