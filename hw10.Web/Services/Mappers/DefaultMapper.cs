using AutoMapper;
using hw10.DAL;

namespace hw10.Services.Mappers
{
    public class DefaultMapper<T, TDto> : IDtoMapper<T, TDto>
    {
        protected virtual IMapper to { get =>
            new MapperConfiguration(cfg => cfg.CreateMap<T, TDto>()).CreateMapper(); }
        protected virtual IMapper from { get =>
            new MapperConfiguration(cfg => cfg.CreateMap<TDto, T>()).CreateMapper(); }

        public virtual TDto ToDto(T model) =>
            to.Map<T, TDto>(model);

        public virtual T FromDto(TDto dto) =>
            from.Map<TDto, T>(dto);

        public virtual T FromDto(TDto dto, T model) =>
            from.Map(dto, model);
    }
}
