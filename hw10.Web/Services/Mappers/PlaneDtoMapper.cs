using AutoMapper;
using hw10.Dto;
using hw10.Models;

namespace hw10.Services.Mappers
{
    public class PlaneDtoMapper : DefaultMapper<Plane, PlaneDto>
    {
        protected override IMapper from { get =>
            new MapperConfiguration(cfg =>
                cfg.CreateMap<PlaneDto, Plane>()
                    .ForMember(
                        p => p.PlaneType,
                        opt => opt.Ignore())
            ).CreateMapper();
        }
    }
}
