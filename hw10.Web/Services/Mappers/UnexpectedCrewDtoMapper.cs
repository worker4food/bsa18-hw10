using System.Linq;
using System;
using System.Collections.Generic;
using AutoMapper;
using hw10.Dto;
using hw10.Models;

namespace hw10.Services.Mappers
{
    public class UnexpectedCrewDtoMapper : DefaultMapper<Crew, UnexpectedCrewDto>
    {
        protected override IMapper from =>
            new MapperConfiguration (cfg => cfg.CreateMap<UnexpectedCrewDto, Crew>()
                .ForMember(dest => dest.Pilot,
                    opt => opt.Ignore())

                .ForMember(dest => dest.CrewStewardesses,
                    opt => opt.MapFrom(src =>
                        src.Stewardesses.Select(s =>
                            new CrewStewardess
                            {
                                Id = 0,
                                CrewId = src.Id ?? 0,
                                StewardessId = s.Id ?? 0
                            })))

                .ForMember(dest => dest.PilotId,
                    opt => opt.MapFrom(src =>
                        src.Pilots
                            .Select(x => x.Id)
                            .FirstOrDefault()))
            ).CreateMapper();

        protected override IMapper to =>
             new MapperConfiguration(cfg =>
                cfg.CreateMap<Crew, UnexpectedCrewDto>()
                    .ForMember(
                        dest => dest.Stewardesses,
                        opt => opt.MapFrom(src =>
                            src.CrewStewardesses.Select(x => x.Stewardess)))
                    .ForMember(
                        dest => dest.Pilots,
                        opt => opt.MapFrom(src =>
                            new List<Pilot> { src.Pilot }))
            ).CreateMapper();
    }
}
