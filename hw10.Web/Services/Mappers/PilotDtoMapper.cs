using AutoMapper;
using hw10.Dto;
using hw10.Models;

namespace hw10.Services.Mappers
{
    public class PilotDtoMapper : DefaultMapper<Pilot, PilotDto>
    {
        protected override IMapper from { get =>
            new MapperConfiguration(cfg => cfg.CreateMap<PilotDto, Pilot>()
                .ForMember(dest => dest.Crew,
                    opt => opt.Ignore())
            ).CreateMapper();
        }
    }
}
