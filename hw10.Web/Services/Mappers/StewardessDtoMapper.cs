using AutoMapper;
using hw10.Dto;
using hw10.Models;

namespace hw10.Services.Mappers
{
    public class StewardessDtoMapper : DefaultMapper<Stewardess, StewardessDto>
    {
        protected override IMapper from { get => new MapperConfiguration(cfg =>
           cfg.CreateMap<StewardessDto, Stewardess>()
                .ForMember(dest => dest.CrewStewardesses,
                    o => o.Ignore())
            ).CreateMapper();
        }
    }
}
