using System.Collections.Generic;
using System.Threading.Tasks;
using hw10.Dto;
using hw10.Models;

namespace hw10.Services
{
    public interface ICrewService : IAsyncAirService<long, Crew, CrewDto>
    {
        Task<IEnumerable<StewardessDto>> GetStewardessList(long id);
        Task<StewardessDto> AddStewardess(long crewId, long id);
        Task<StewardessDto> RemoveStewardess(long crewId, long id);
    }
}
