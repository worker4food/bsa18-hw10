using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using hw10.DAL;
using hw10.Models;
using hw10.Exceptions;
using hw10.Services.Mappers;

namespace hw10.Services
{
    public class AsyncDummyAirService<TKey, T, TDto> : IAsyncAirService<TKey, T, TDto>
        where T : Entity<TKey>
        //where TDto : class
    {
        protected IAsyncRepository<TKey, T> repo;
        protected IDtoMapper<T, TDto> mapper;

        public AsyncDummyAirService(IAsyncRepository<TKey, T> repo, IDtoMapper<T, TDto> m)
        {
            this.repo = repo;
            this.mapper = m;
        }

        virtual public async Task<IEnumerable<TDto>> GetList() =>
            await repo.GetAll().AsNoTracking()
                .Select(x => mapper.ToDto(x))
                .ToListAsync();

        async virtual public Task<TDto> GetById(TKey id)
        {
            var res = await repo.Get(id);

            if(res != null)
                return mapper.ToDto(res);
            else
                throw NotFoundEx(id);
        }

        async virtual public Task<TDto> CreateNew(TDto item)
        {
            var newEntity = mapper.FromDto(item);

            await repo.Insert(newEntity);

            return mapper.ToDto(newEntity);
        }

        async virtual public Task<TDto> UpdateById(TKey id, TDto item)
        {
            var o = await repo.Get(id);

            if (o == null)
                throw NotFoundEx(id);

            var newE = mapper.FromDto(item, o);

            newE.Id = id;
            await repo.Update(newE);

            return mapper.ToDto(newE);
        }

        virtual public Task DeleteById(TKey id)
        {
            if (repo.Get(id) != null)
                return repo.Delete(id);
            else
                throw NotFoundEx(id);
        }

        protected Exception NotFoundEx(TKey id) =>
            new EntityNotFoundException($"Entity {typeof(T).Name} with id <{id}> not found");
    }
}
