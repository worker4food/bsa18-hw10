using hw10.Dto;
using hw10.Models;
using hw10.DAL;
using hw10.Services.Mappers;

namespace hw10.Services
{
    public class PilotService : AsyncDummyAirService<long, Pilot, PilotDto>, IPilotService
    {
        public PilotService(IAsyncRepository<long, Pilot> repo, IDtoMapper<Pilot, PilotDto> m) : base(repo, m)
        {
        }
    }
}
