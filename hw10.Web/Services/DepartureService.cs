using System;
using System.Threading.Tasks;
using hw10.Dto;
using hw10.Models;
using hw10.DAL;
using hw10.Services.Mappers;

namespace hw10.Services
{
    public class DepartureService : AsyncDummyAirService<long, Departure, DepartureDto>, IDepartureService
    {
        public DepartureService(
            IAsyncRepository<long, Departure> repo,
            IDtoMapper<Departure, DepartureDto> m) : base(repo, m)
        {
        }

        public async Task<DepartureDto> MakeDeparture(long id)
        {
            var d = await repo.Get(id);

            if(d == null)
                throw NotFoundEx(id);

            d.DepartureDate = DateTime.Now;
            await repo.Update(d);

            return mapper.ToDto(d);
        }

        public override Task<DepartureDto> CreateNew(DepartureDto item)
        {
            item.DepartureDate = default(DateTimeOffset);
            return base.CreateNew(item);
        }

        public override Task<DepartureDto> UpdateById(long id, DepartureDto item)
        {
            item.DepartureDate = default(DateTimeOffset);
            return base.CreateNew(item);
        }
    }
}
