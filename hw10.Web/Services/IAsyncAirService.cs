using System.Collections.Generic;
using System.Threading.Tasks;
using hw10.Models;

namespace hw10.Services
{
    public interface IAsyncAirService<TKey, T, TDto>
        where T : Entity<TKey>
    {
        Task<IEnumerable<TDto>> GetList();

        Task<TDto> GetById(TKey id);

        Task<TDto> CreateNew(TDto item);

        Task<TDto> UpdateById(TKey id, TDto item);

        Task DeleteById(TKey id);

    }
}
