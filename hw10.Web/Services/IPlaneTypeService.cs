using hw10.Dto;
using hw10.Models;

namespace hw10.Services
{
    public interface IPlaneTypeService: IAsyncAirService<long, PlaneType, PlaneTypeDto> { }
}
