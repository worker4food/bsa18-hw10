using System;
using System.Linq;
using hw10.Dto;
using hw10.Models;
using hw10.DAL;
using hw10.Services.Mappers;

namespace hw10.Services
{
    public class PlaneTypeService: AsyncDummyAirService<long, PlaneType, PlaneTypeDto>, IPlaneTypeService
    {
        public PlaneTypeService(IAsyncRepository<long, PlaneType> r, IDtoMapper<PlaneType, PlaneTypeDto> m) : base(r, m)
        {}
    }
}
