using System;
using System.Linq;
using hw10.Dto;
using hw10.Models;
using hw10.DAL;
using hw10.Services.Mappers;

namespace hw10.Services
{
    public class PlaneService: AsyncDummyAirService<long, Plane, PlaneDto>, IPlaneService
    {
        public PlaneService(IAsyncRepository<long, Plane> r, IDtoMapper<Plane, PlaneDto> m) : base(r, m)
        {}
    }
}
