using hw10.Dto;
using hw10.Models;
using hw10.DAL;
using hw10.Services.Mappers;

namespace hw10.Services
{
    public class StewardessService : AsyncDummyAirService<long, Stewardess, StewardessDto>, IStewardessService
    {
        public StewardessService(IAsyncRepository<long, Stewardess> repo, IDtoMapper<Stewardess, StewardessDto> m) : base(repo, m)
        {
        }
    }
}
