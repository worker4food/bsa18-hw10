using hw10.Dto;
using hw10.Models;

namespace hw10.Services
{
    public interface IStewardessService : IAsyncAirService<long, Stewardess, StewardessDto> {}
}
