using System.Threading.Tasks;
using hw10.Dto;
using hw10.Models;

namespace hw10.Services
{
    public interface IFlightService: IAsyncAirService<string, Flight, FlightDto> {
        Task<FlightDto> CreateNew(FlightSchedDto p);
        Task<TicketDto> BuyTicket(string flightId, TicketPrefDto opt);
        Task ReturnTicket(string flightId, TicketDto ticket);

    }
}
