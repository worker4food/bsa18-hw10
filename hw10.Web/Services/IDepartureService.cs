using System.Threading.Tasks;
using hw10.Dto;
using hw10.Models;

namespace hw10.Services
{
    public interface IDepartureService : IAsyncAirService<long, Departure, DepartureDto>
    {
        Task<DepartureDto> MakeDeparture(long id);
    }
}
