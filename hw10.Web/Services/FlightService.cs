using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using hw10.Dto;
using hw10.Models;
using hw10.DAL;
using hw10.Services.Mappers;

using TicketKind = hw10.Dto.TicketPrefDto.KindEnum;

namespace hw10.Services
{
    public class FlightService: AsyncDummyAirService<string, Flight, FlightDto>, IFlightService
    {
        protected IAsyncRepository<long, Ticket> ticketRepo;
        protected IDtoMapper<Ticket, TicketDto> ticketMapper;
        public FlightService(
            IAsyncRepository<string, Flight> r,
            IAsyncRepository<long, Ticket> ticketRepo,
            IDtoMapper<Flight, FlightDto> m,
            IDtoMapper<Ticket, TicketDto> ticketMapper) : base(r, m)
        {
            this.ticketRepo = ticketRepo;
            this.ticketMapper = ticketMapper;
        }

        public async Task<FlightDto> CreateNew(FlightSchedDto flightSched)
        {
            var newId = Guid.NewGuid().GetHashCode().ToString("x").PadLeft(8, '0');
            var newF = mapper.FromDto(flightSched.Flight);

            newF.Tickets = flightSched.TicketParams
                .SelectMany(x =>
                    Enumerable.Range(1, x.Count ?? 0)
                    .Select(_ => new Ticket {
                        Flight = newF,
                        Price = x.Price ?? 0
                    }))
                    .ToList();

            newF.Id = newId;
            await repo.Insert(newF);

            return mapper.ToDto(newF);
        }

        public async Task<TicketDto> BuyTicket(string flightId, TicketPrefDto opt)
        {
            var tickets = (
                    await ticketRepo.GetAll()
                    .Where(x => x.Flight.Id == flightId)
                    .ToListAsync())
                .AsQueryable();

            if(opt.Kind == TicketKind.MaxPriceEnum)
                tickets = tickets.OrderByDescending(x => x.Price);
            else if(opt.Kind == TicketKind.MinPriceEnum)
                tickets = tickets.OrderBy(x => x.Price);

            var res = await tickets.FirstAsync();

            await ticketRepo.Delete(res.Id);

            return ticketMapper.ToDto(res);
        }

        public Task ReturnTicket(string flightId, TicketDto ticket)
        {
            ticket.FlightId = flightId;
            var newTicket = ticketMapper.FromDto(ticket);

            return ticketRepo.Insert(newTicket);
        }
    }
}
