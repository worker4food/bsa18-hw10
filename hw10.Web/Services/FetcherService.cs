using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using hw10.Models;
using hw10.Dto;
using hw10.DAL;
using hw10.Services.Mappers;

namespace hw10.Services
{
    public interface IFetcherService
    {
        Task<IEnumerable<UnexpectedCrewDto>> FetchCrewData(int count);
    }

    public class FetcherService : IFetcherService
    {
        protected readonly string resourceUrl = "http://5b128555d50a5c0014ef1204.mockapi.io/crew";
        protected IDtoMapper<Crew, UnexpectedCrewDto> crewMapper;
        protected IDtoMapper<Pilot, PilotDto> pilotMapper;
        protected IDtoMapper<Stewardess, StewardessDto> stewardessMapper;
        protected IAsyncRepository<long, Crew> crewRepo;
        protected IAsyncRepository<long, Pilot> pilotRepo;
        protected IAsyncRepository<long, Stewardess> stewardessRepo;
        protected IAsyncRepository<long, CrewStewardess> crewStewRepo;
        public FetcherService(
            IAsyncRepository<long, Crew> crewRepo,
            IAsyncRepository<long, Pilot> pilotRepo,
            IAsyncRepository<long, Stewardess> stewardessRepo,
            IAsyncRepository<long, CrewStewardess> crewStewRepo,
            IDtoMapper<Crew, UnexpectedCrewDto> crewMapper,
            IDtoMapper<Pilot, PilotDto> pilotMapper,
            IDtoMapper<Stewardess, StewardessDto> stewardessMapper)
        {
            this.crewRepo = crewRepo;
            this.pilotRepo = pilotRepo;
            this.stewardessRepo = stewardessRepo;
            this.crewStewRepo = crewStewRepo;

            this.crewMapper = crewMapper;
            this.pilotMapper = pilotMapper;
            this.stewardessMapper = stewardessMapper;
        }

        protected async Task<IEnumerable<Crew>> SaveToDbAsync(IEnumerable<UnexpectedCrewDto> crews)
        {
            var pilotsMergeTask = Task.WhenAll(crews
                .SelectMany(x => x.Pilots)
                .Select(pilotMapper.FromDto)
                .Select(pilotRepo.Merge));

            var stewardessesMergeTask = Task.WhenAll(crews
                .SelectMany(x => x.Stewardesses)
                .Select(stewardessMapper.FromDto)
                .Select(stewardessRepo.Merge));

            // This not works, why?
            //  System.InvalidOperationException: A second operation started on this context
            //  before a previous operation completed. Any instance members are not guaranteed
            //  to be thread safe
            // OKAY
            // var crewStewCleanupTask = Task.WhenAll(
            //     from csDb in crewStewRepo.GetAllNoTracking()
            //     join c in crews on csDb.CrewId equals c.Id
            //     select crewStewRepo.Delete(csDb.Id));
            // await crewStewCleanupTask;

            // And not works with pre-clearing CrewStewardesses
            var crewStewsToRemove = await (
               from csDb in crewStewRepo.GetAllNoTracking()
               join c in crews on csDb.CrewId equals c.Id
               select csDb.Id).Distinct().ToListAsync();

            foreach(var csId in crewStewsToRemove)
                await crewStewRepo.Delete(csId);

            await Task.WhenAll(pilotsMergeTask, stewardessesMergeTask);


            var result = await Task.WhenAll(crews
                .Select(crewMapper.FromDto)
                .Select(crewRepo.Merge));

            return result;
        }

        protected async Task SaveToFileAsync(IEnumerable<UnexpectedCrewDto> crews)
        {
            Directory.CreateDirectory("csv");
            var filename = "csv/" + DateTime.UtcNow.ToString("yyyyMMdd_HHmmssfff") + ".csv";

            using(var fs = new FileStream(filename, FileMode.Append, FileAccess.Write))
            using(var writer = new StreamWriter(fs))
            {
                await writer.WriteLineAsync($"CrewId, PilotId, PilotFirsName, PilotLastName, PilotBirthDate, PilotExp");
                foreach(var c in crews) {
                    var p = c.Pilots.FirstOrDefault();
                    await writer.WriteLineAsync($"{c.Id},{p.Id},{p?.FirstName},{p?.LastName},{p?.BirthDate},{p?.Experience}");
                }
            }
        }

        public async Task<IEnumerable<UnexpectedCrewDto>> FetchCrewData(int count = 10)
        {
            IEnumerable<UnexpectedCrewDto> src;

            using(var client = new HttpClient())
            {
                var data = await client.GetStringAsync(resourceUrl).ConfigureAwait(false);

                src = JsonConvert
                    .DeserializeObject<IEnumerable<UnexpectedCrewDto>>(data)
                    .Take(count);
            }

            var updateDb = SaveToDbAsync(src);
            await Task.WhenAll(updateDb, SaveToFileAsync(src));

            //Select with stewardesses!
            var crews = await Task.WhenAll((await updateDb)
                    .Select(x => x.Id)
                    .Select(crewRepo.GetNoTracking));

            return crews.Select(crewMapper.ToDto);
        }
    }
}
