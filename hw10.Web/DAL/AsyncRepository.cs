using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using hw10.Models;

namespace hw10.DAL
{
    public class AsyncRepository<TKey, T> : IAsyncRepository<TKey, T>
        where T : Entity<TKey>
    {
        private DbContext db;
        DbSet<T> table;

        public AsyncRepository(AirportEfDb db)
        {
            this.db = db;
            table = db.Set<T>();
        }

        protected IQueryable<T> withNavProps(IQueryable<T> query, bool noTrack = false)
        {
            foreach (var property in db.Model.FindEntityType(typeof(T)).GetNavigations())
            {
                query = query.Include(property.Name);
                if(noTrack)
                    query = query.AsNoTracking();
            }

            return query;
        }

        public IQueryable<T> GetAll() =>
            withNavProps(table);

        public IQueryable<T> GetAllNoTracking() =>
            withNavProps(table.AsNoTracking(), true);

        public Task<T> Get(TKey id) =>
            withNavProps(table)
                .FirstOrDefaultAsync(x => x.Id.Equals(id));

        public Task<T> GetNoTracking(TKey id) =>
            withNavProps(table, true)
                .FirstOrDefaultAsync(x => x.Id.Equals(id));

        async public Task<T> Insert(T item) {
            await table.AddAsync(item);
            await Save();

            return item;
        }

        async public Task Delete(TKey id) {
            table.Remove(await db.FindAsync<T>(id));
            await Save();
        }

        async public Task<T> Update(T item) {
            table.Update(item);
            await Save();

            return item;
        }

        async public Task<T> Merge(T item)
        {   var entity = await db.FindAsync<T>(item.Id);

            if(entity != null)
                db.Remove(entity);

            return await Insert(item);
        }

        public Task Save() =>
            db.SaveChangesAsync();
    }
}
