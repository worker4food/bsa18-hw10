using System.Linq;
using System.Threading.Tasks;

namespace hw10.DAL
{
    public interface IAsyncRepository<TKey, T>
    {
        IQueryable<T> GetAll();
        IQueryable<T> GetAllNoTracking();
        Task<T> GetNoTracking(TKey id);
        Task<T> Get(TKey id);
        Task<T> Insert(T item);
        Task<T> Update(T item);
        Task<T> Merge(T item);
        Task Delete(TKey id);
        Task Save();

    }
}
