using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System;
using hw10.Models;
using hw10.Utils;

namespace hw10.DAL
{
    public class AirportEfDb : DbContext
    {
        public DbSet<PlaneType> PlaneType { get; set; }
        public DbSet<Plane> Plane { get; set; }
        public DbSet<Pilot> Pilot { get; set; }
        public DbSet<Stewardess> Stewardess { get; set; }
        public DbSet<Crew> Crew { get; set; }
        public DbSet<Flight> Flight { get; set; }
        public DbSet<Departure> Departure { get; set; }
        public DbSet<Ticket> Ticket { get; set; }
        public DbSet<CrewStewardess> CrewStewardess { get; set; }

        public AirportEfDb(DbContextOptions<AirportEfDb> opt) : base(opt)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Pilot>()
                .HasOne<Crew>(x => x.Crew)
                .WithOne(x => x.Pilot)
                .HasForeignKey<Crew>(x => x.PilotId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Crew>()
                .HasMany<CrewStewardess>(x => x.CrewStewardesses)
                .WithOne(x => x.Crew)
                .HasForeignKey(x => x.StewardessId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<CrewStewardess>()
                .HasOne<Crew>(x => x.Crew)
                .WithMany(x => x.CrewStewardesses)
                .HasForeignKey(x => x.CrewId);

            modelBuilder.Entity<CrewStewardess>()
                .HasOne<Stewardess>(x => x.Stewardess)
                .WithMany(x => x.CrewStewardesses)
                .HasForeignKey(x => x.StewardessId);
                //.OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Plane>()
                .HasOne<PlaneType>(x => x.PlaneType)
                .WithMany()
                .HasForeignKey(x => x.PlaneTypeId);

            modelBuilder.Entity<Departure>()
                .HasOne<Plane>(x => x.Plane)
                .WithOne()
                .HasForeignKey<Departure>(x => x.PlaneId);

            modelBuilder.Entity<Departure>()
                .HasOne<Flight>(x => x.Flight)
                .WithOne()
                .HasForeignKey<Departure>(x => x.FlightId);

            modelBuilder.Entity<Departure>()
                .HasOne<Crew>(x => x.Crew)
                .WithOne()
                .HasForeignKey<Departure>(x => x.CrewId);

            modelBuilder.Entity<Flight>()
                .HasMany<Ticket>(x => x.Tickets)
                .WithOne(x => x.Flight)
                .HasForeignKey(x => x.FlightId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
